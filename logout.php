<?php
    /**
    @author Ruhland Matthias

    @date 16.05.2008
    @file logout.php
    
    @brief Logout from the moviecollection
    */

    unset($_SESSION['userlevel']);
    unset($_SESSION['user']);
    unset($_SESSION['ip']);
    session_destroy();
    echo $language['mc_loggedout'] , '<br />' , "\n";
    echo $language['mc_reload1'];
    echo '<a href="index.php?section=mc_mainpage">' , $language['mc_reload2'] , '</a>' , "\n";
    echo $language['mc_reload3'] , '<br />' , "\n";
?>