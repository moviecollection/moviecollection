<?php
    /**
    @author Ruhland Matthias

    @date 16.05.2008
    @file search.php
    
    @brief Search a movie in the database
    */

    if (isset($_SESSION['userlevel']) === true) {
    
        if ($_SESSION['userlevel'] >= 1 && $_SESSION['ip'] === $_SERVER['REMOTE_ADDR']) {
            echo '<h1 id="title">' , $language['mc_search'] , '</h1>' , "\n";

            echo '<p>' , $language['mc_search_for'] , '</p>' , "\n";

            function search($find) {
                include 'settings/settings.php';    // File with settings
                include $mc_language;               // Include language

                $db  = mysql_connect($mc_db_host, $mc_db_user, $mc_db_pass);
                if ($db === false) {
                    echo $language['mc_err_connection'] , '<br />' , "\n";
                    return;
                }
                
                $db_change = mysql_query('use ' , $mc_db_database);
                if ($db_change === false) {
                    echo $language['mc_use_database'] , '<br />' , "\n";
                    return;
                }
                
                $res = mysql_query("select * from $mc_db_table_movie where $find");
                if ($res === false) {
                    echo $language['mc_err_load_movie'] , '<br />' , "\n";
                    return;
                }
                
                $num = mysql_num_rows($res);
                echo $num , ' ' , $language['mc_entries'] , '<br />' , "\n";

                echo '<table>' , "\n";
                echo '<tr>' , "\n";
                echo '<th>' , $language['mc_id'] , '</th>' , "\n";
                echo '<th>' , $language['mc_movie'] , '</th>' , "\n";
                echo '<th>' , $language['mc_genre'] , '</th>' , "\n";
                if ($mc_genre2 === true)
                    echo '<th>' , $language['mc_genre2'] , '</th>' , "\n";
                
                echo '<th>' , $language['mc_description'] , '</th>' , "\n";
                if ($mc_release === true)
                    echo '<th>' , $language['mc_relese'] , '</th>' , "\n";
                
                if ($mc_rating === true)
                    echo '<th>' , $language['mc_rating'] , '</th>' , "\n";
                
                if ($mc_medium === true)
                    echo '<th>' , $language['mc_medium'] , '</th>' , "\n";
                
                if ($mc_quantity === true)
                    echo '<th>' , $language['mc_quantaty'] , '</th>' , "\n";
                
                if ($mc_format === true)
                    echo '<th>' , $language['mc_format'] , '</th>' , "\n";
                
                if ($mc_place === true)
                    echo '<th>' , $language['mc_place'] , '</th>' , "\n";
                
                echo '</tr>' , "\n";

                for ($i = 0; $i < $num; $i++) {
                    $id    = mysql_result($res, $i, 'id');
                    $movie = mysql_result($res, $i, 'movie');
                    $genre = mysql_result($res, $i, 'genre');
                    if ($mc_genre2 === true)
                        $genre2 = mysql_result($res, $i, 'genre2');
                    
                    $description = mysql_result($res, $i, 'description');
                    if ($mc_release === true)
                        $release = mysql_result($res, $i, 'rel');
                    
                    if ($mc_rating === true)
                        $rating = mysql_result($res, $i, 'rating');
                    
                    if ($mc_medium === true)
                        $medium = mysql_result($res, $i, 'medium');
                    
                    if ($mc_quantity === true)
                        $quantity = mysql_result($res, $i, 'quantity');
                    
                    if ($mc_format === true)
                        $format = mysql_result($res, $i, 'format');
                    
                    if ($mc_place === true)
                        $place = mysql_result($res, $i, 'place');
                    
                    echo '<tr>' , "\n";
                    echo '<td>' , $id , '</td>' , "\n";
                    echo '<td>' , $movie , '</td>' , "\n";
                    echo '<td>' , $genre , '</td>' , "\n";
                    if ($mc_genre2 === true)
                        echo '<td>' , $genre2 , '</td>' , "\n";
                    
                    echo '<td>' , $description , '</td>' , "\n";
                    if ($mc_release === true)
                        echo '<td>' , $release , '</td>' , "\n";
                    
                    if ($mc_rating === true)
                        echo '<td>' , $rating , '</td>' , "\n";
                    
                    if ($mc_medium === true)
                        echo '<td>' , $medium , '</td>' , "\n";
                    
                    if ($mc_quantity === true)
                        echo '<td>' , $quantity , '</td>' , "\n";
                    
                    if ($mc_format === true)
                        echo '<td>' , $format , '</td>' , "\n";
                    
                    if ($mc_place === true)
                        echo '<td>' , $place , '</td>' , "\n";
                    
                    echo '</tr>' , "\n";
                }
                echo '</table>' , "\n";
                
                mysql_close($db);
            }

            echo '<form action="index.php?section=mc_search" method="POST">' , "\n";
            echo '    <input type="submit" value="' , $language['mc_id'] , '" name="search_id" />' , "\n";
            echo '    <input type="submit" value="' , $language['mc_movie'] , '" name="search_movie" />' , "\n";
            echo '    <input type="submit" value="' , $language['mc_genre'] , '" name="search_genre" />' , "\n";
            echo '    <input type="submit" value="' , $language['mc_genre2'] , '" name="search_genre2" />' , "\n";
            echo '    <input type="submit" value="' , $language['mc_description'] , '" name="search_description" />' , "\n";
            //echo '    <input type="submit" value="' , $language['mc_relese'] , '" name="search_release" />' , "\n";
            echo '    <input type="submit" value="' , $language['mc_rating'] , '" name="search_rating" />' , "\n";
            echo '    <input type="submit" value="' , $language['mc_medium'] , '" name="search_medium" />' , "\n";
            echo '    <input type="submit" value="' , $language['mc_quantaty'] , '" name="search_quantity" />' , "\n";
            echo '    <input type="submit" value="' , $language['mc_format'] , '" name="search_format" />' , "\n";
            echo '    <input type="submit" value="' , $language['mc_place'] , '" name="search_place" />' , "\n";
            echo '</form>' , "\n";
            echo '<p>' , "\n";

            if (isset($_POST['search_id']) === true) {
                echo '<form action="index.php?section=mc_search" method="POST">' , "\n";
                echo $language['mc_id'] , ': <input name="id" size="3" /><br />' , "\n";
                echo '<input type="submit" value="' , $language['mc_search_movie'] , '" name="search_for_id" />' , "\n";
                echo '</form>' , "\n";
            }

            if (isset($_POST['search_movie']) === true) {
                echo '<form action="index.php?section=mc_search" method="POST">' , "\n";
                echo $language['mc_movie'] , ': <input name="movie" size="20" /><br />' , "\n";
                echo '<input type="submit" value="' , $language['mc_search_movie'] , '" name="search_for_movie" />' , "\n";
                echo '</form>' , "\n";
            }

            if (isset($_POST['search_genre']) === true) {
                echo '<form action="index.php?section=mc_search" method="POST">' , "\n";
                echo $language['mc_genre'] , ': <input name="genre" size="20" /><br />' , "\n";
                echo '<input type="submit" value="' , $language['mc_search_movie'] , '" name="search_for_genre" />' , "\n";
                echo '</form>' , "\n";
            }

            if (isset($_POST['search_genre2']) === true) {
                echo '<form action="index.php?section=mc_search" method="POST">' , "\n";
                echo $language['mc_genre2'] , ': <input name="genre2" size="20" /><br />' , "\n";
                echo '<input type="submit" value="' , $language['mc_search_movie'] , '" name="search_for_genre2" />' , "\n";
                echo '</form>' , "\n";
            }

            if (isset($_POST['search_description']) === true) {
                echo '<form action="index.php?section=mc_search" method="POST">' , "\n";
                echo $language['mc_description'] , ': <input name="description" size="20" /><br />' , "\n";
                echo '<input type="submit" value="' , $language['mc_search_movie'] , '" name="search_for_description" />' , "\n";
                echo '</form>' , "\n";
            }

            if (isset($_POST['search_release']) === true) {
                echo '<form action="index.php?section=mc_search" method="POST">' , "\n";
                echo $language['mc_relese'] , ': <input name="release" size="10" /><br />' , "\n";
                echo '<input type="submit" value="' , $language['mc_search_movie'] , '" name="search_for_release" />' , "\n";
                echo '</form>' , "\n";
            }

            if (isset($_POST['search_rating']) === true) {
                echo '<form action="index.php?section=mc_search" method="POST">' , "\n";
                echo $language['mc_rating'] , ': <input name="rating" size="2" /><br />' , "\n";
                echo '<input type="submit" value="' , $language['mc_search_movie'] , ' name="search_for_rating" />' , "\n";
                echo '</form>' , "\n";
            }

            if (isset($_POST['search_medium']) === true) {
                echo '<form action="index.php?section=mc_search" method="POST">' , "\n";
                echo $language['mc_medium'] , ': <input name="medium" size="3" /><br />' , "\n";
                echo '<input type="submit" value="' , $language['mc_search_movie'] , '" name="search_for_medium" />' , "\n";
                echo '</form>' , "\n";
            }

            if (isset($_POST['search_quantity']) === true) {
                echo '<form action="index.php?section=mc_search" method="POST">' , "\n";
                echo $language['mc_quantaty'] , ': <input name="quantity" size="1" /><br />' , "\n";
                echo '<input type="submit" value="' , $language['mc_search_movie'] , '" name="search_for_quantity" />' , "\n";
                echo '</form>' , "\n";
            }

            if (isset($_POST['search_format']) === true) {
                echo '<form action="index.php?section=mc_search" method="POST">' , "\n";
                echo $language['mc_format'] , ': <input name="format" size="20" /><br />' , "\n";
                echo '<input type="submit" value="' , $language['mc_search_movie'] , '" name="search_for_format" />' , "\n";
                echo '</form>' , "\n";
            }

            if (isset($_POST['search_place']) === true) {
                echo '<form action="index.php?section=mc_search" method="POST">' , "\n";
                echo $language['mc_place'] , ': <input name="place" size="20" /><br />' , "\n";
                echo '<input type="submit" value="' , $language['mc_search_movie'] , '" name="search_for_place" />' , "\n";
                echo '</form>' , "\n";
            }

            if (isset($_POST['search_for_id']) === true)
                search("id = '" , mysql_real_escape_string($_POST['id']) , "'");

            if (isset($_POST['search_for_movie']) === true)
                search("movie like '%" , mysql_real_escape_string($_POST['movie']) , "%'");

            if (isset($_POST['search_for_genre']) === true)
                search("genre like '%" , mysql_real_escape_string($_POST['genre']) , "%'");

            if (isset($_POST['search_for_genre2']) === true)
                search("genre2 like '%" , mysql_real_escape_string($_POST['genre2']) , "%'");

            if (isset($_POST['search_for_description']) === true)
                search("description like '%" , mysql_real_escape_string($_POST['description']) , "%'");

            if (isset($_POST['search_for_release']) === true)
                search("release like '%" , mysql_real_escape_string($_POST['release']) , "%'");

            if (isset($_POST['search_for_rating']) === true)
                search("rating like '%" , mysql_real_escape_string($_POST['rating']) , "%'");

            if (isset($_POST['search_for_medium']) === true)
                search("medium like '%" , mysql_real_escape_string($_POST['medium']) , "%'");

            if (isset($_POST['search_for_quantity']) === true)
                search("quantity like '%" , mysql_real_escape_string($_POST['quantity']) , "%'");

            if (isset($_POST['search_for_format']) === true)
                search("format like '%" , mysql_real_escape_string($_POST['format']) , "%'");

            if (isset($_POST['search_for_place']) === true)
                search("place like '%" , mysql_real_escape_string($_POST['ort']) , "%'");
        }
        else
            echo $language['mc_not_loggedin'] , "\n";
        
    }
?>