<?php
    /**
    @author Ruhland Matthias

    @date 16.05.2008
    @file login.php
    
    @brief Login the user into the movie collection
    */

    echo '<h1 id="title">' , $language['mc_menu_login'] , '</h1>' , "\n";

    if (isset($_POST['login']) === false) {
        echo '<form action="index.php?section=mc_login" method="POST">' , "\n";
        echo $language['mc_username'] , '<input name="username" size="30" maxlength="30" /><br />' , "\n";
        echo $language['mc_password'] , '<input name="password" type="password" size="30" /><br />' , "\n";
        echo '    <input type="submit" value="' , $language['mc_loggingin'] , '" name="login" />' , "\n";
        echo '    <input type="reset" value="' , $language['mc_clear'] , '" />' , "\n";
        echo '</form>' , "\n";
    }

    if (isset($_POST['login']) === true) {
        include 'password.inc.php';
        
        $db = mysql_connect($mc_db_host, $mc_db_user, $mc_db_pass);
        if ($db === false) {
            echo $language['mc_err_connection'] , '<br />' , "\n";
            return;
        }
        
        $db_change = mysql_query('use ' , $mc_db_database);
        if ($db_change === false) {
            echo $language['mc_use_database'] , '<br />' , "\n";
            return;
        }
        
        $user        = mysql_real_escape_string($_POST['username']);
        $res         = mysql_db_query($mc_db_database, "select * from $mc_db_table_users where username='$user'");
        if ($res === false) {
            echo $language['mc_err_load_movie'] , '<br />' , "\n";
            return;
        }
        
        $db_user     = mysql_result($res, 0, 'username');
        $db_password = mysql_result($res, 0, 'userpass');
        $db_level    = mysql_result($res, 0, 'userlevel');
        
        if ($user === $db_user && password_check($_POST['password'], $db_password) === true) {
            // Write to the session
            $_SESSION['userlevel'] = $db_level;
            $_SESSION['user']      = $db_user;
            $_SESSION['ip']        = $_SERVER['REMOTE_ADDR'];
            echo $language['mc_reload1'];
            echo '<a href="index.php?section=mc_mainpage">' , $language['mc_reload2'] , '</a>';
            echo $language['mc_reload3'] , '<br />' , "\n";
        } else
            echo '<p>' , $language['mc_err_user'] , '</p>' , "\n";
        
        mysql_close($db);
    }
?>