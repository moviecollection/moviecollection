<?php
    /**
    @author Ruhland Matthias

    @date 16.05.2008
    @file settings.php
    
    @brief File with all settings
    */

    /**
    @brief Language for the page
    
    The files are stored in "/lang" directory
    */
    $mc_language       = 'lang/de.php';

    // Dabase
    $mc_db_host        = 'localhost';               //!< Host to the Database-Server
    $mc_db_user        = 'test_mc';                 //!< Username for the Database
    $mc_db_pass        = 'test_mc';                 //!< Password for the Database
    $mc_db_database    = 'test_moviecollection';    //!< Database for the Moviecollection
    $mc_db_table_movie = 'mc_movies';               //!< Table for movies
    $mc_db_table_users = 'mc_users';                //!< Table for users
    
    /**
    @brief If the database exists on the server
    
    The installer will try to create the database, if it doesn't exist.
    If there is a database, this will fail and the installation will also fail.
    On most webspaces there will be a database for you, on your on server not.
    
    Possible Values:
    - true
    - false
    */
    $mc_db_exists      = false;
    
    // Additional Columns
    $mc_genre2         = true;
    $mc_release        = true;
    $mc_rating         = true;
    $mc_medium         = true;
    $mc_quantity       = true;
    $mc_format         = true;
    $mc_place          = true;
?>