<?php
    /**
    @author Ruhland Matthias

    @date 16.05.2008
    @file settings/menu.php
    
    @brief Contains the menu
    */

    if (isset($_SESSION['ip']) === true) {
        if ($_SESSION['ip'] === $_SERVER['REMOTE_ADDR'])
            $userlevel = $_SESSION['userlevel'];
    } else 
        $userlevel = 0;
    
    echo '<ul>' , "\n";
    
    $sessionOK = ($_SESSION['ip'] === $_SERVER['REMOTE_ADDR']);
    
    // not logged in
    if ($userlevel === 0) {
        echo '<li><a href="index.php?section=mc_login">' ,    $language['mc_menu_login'] , '</a></li>' , "\n";
        echo '<li><a href="index.php?section=mc_mainpage">' , $language['mc_menu_mainpage'] , '</a></li>' , "\n";
    }
    
    // Logged in, but only see the entries
    if ($userlevel >= 1 && $sessionOK === true) {
        echo '<li><a href="index.php?section=mc_logout">' ,   $language['mc_menu_logout'] , '</a></li>' , "\n";
        echo '<li><a href="index.php?section=mc_mainpage">' , $language['mc_menu_mainpage'] , '</a></li>' , "\n";
        echo '<h2 id="title">' , $language['mc_menu_overview'] , '</h2>' , "\n";
        echo '<li><a href="index.php?section=mc_overview">' , $language['mc_menu_overview'] , '</a></li>' , "\n";
        echo '<li><a href="index.php?section=mc_all">' ,      $language['mc_menu_allFilms'] , '</a></li>' , "\n";
        echo '<li><a href="index.php?section=mc_search">' ,   $language['mc_menu_searchFilm'] , '</a></li>' , "\n";
    }
    
    // Logged in and can edit movies
    if ($userlevel >= 2 && $sessionOK === true) {
        echo '<h2 id="title">' , $language['mc_menu_change'] , '</h2>' , "\n";
        echo '<li><a href="index.php?section=mc_add">' ,    $language['mc_menu_add'] , '</a></li>' , "\n";
        echo '<li><a href="index.php?section=mc_change">' , $language['mc_menu_change'] , '</a></li>' , "\n";
        echo '<li><a href="index.php?section=mc_delete">' , $language['mc_menu_delete'] , '</a></li>' , "\n";
    }
    
    // Logged, every user can change his own password
    if ($userlevel >= 1 && $sessionOK === true) {
        echo '<h2 id="title">' , $language['mc_menu_user_overview'] , '</h2>' , "\n";
        echo '<li><a href="index.php?section=mc_pwChange">' , $language['mc_menu_pwd_change'] , '</a></li>' , "\n";
    }
    
    // Logged in and can also edit other users
    if( $userlevel >= 3 && $sessionOK === true) {
        echo '<li><a href="index.php?section=mc_user_overview">' , $language['mc_menu_user_overview'] , '</a></li>' , "\n";
        echo '<li><a href="index.php?section=mc_user_add">' ,      $language['mc_mneu_user_add'] , '</a></li>' , "\n";
        echo '<li><a href="index.php?section=mc_user_change">' ,  $language['mc_menu_user_change'] , '</a></li>' , "\n";
        echo '<li><a href="index.php?section=mc_user_delete">' ,  $language['mc_menu_user_delete'] , '</a></li>' , "\n";
    }
    
    echo '</ul>' , "\n";
?>