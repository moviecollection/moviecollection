<?php
    /**
    @author Ruhland Matthias

    @date 16.05.2008
    @file settings/selection.php
    
    @brief Select the page to include
    */

    if(isset($_GET['section']) === true && isset($links[$_GET['section']] === true))
        include $links[$_GET['section']];
    else
        include $links['mc_mainpage'];
?>