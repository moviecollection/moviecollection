<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<meta http-equiv="pragma" content="no-cache">
<link rel="stylesheet" type="text/css" href="../settings/moviecollection.css">
<?php
    /**
    @author Ruhland Matthias

    @date 16.05.2008
    @file install/install_2.php
    
    @brief Create the tables
    
    @changes:
    08.01.2013    matthias    translated
    08.01.2013    matthias    charset ISO-8859-1 -> UTF-8
    */

    include '../settings/settings.php';     // File with settings
    include '../' , $mc_language;           // Include language
    include '../password.inc.php';          // Create and verify the password
    
    echo '<title>' , $language['mc_title'] , '</title>' , "\n";
    echo '</head' , "\n";
    echo '<body>' , "\n";

    echo '<link rel="stylesheet" type="text/css" href="../settings/moviecollection.css">' , "\n";   // Stylesheet

    echo '<h1 id="title">' , $language['mc_installation'] , '</h1><p>' , "\n";

    if (isset($_POST['create']) === false) {
        // Create connection
        $db = mysql_connect($mc_db_host, $mc_db_user, $mc_db_pass);
        if ($db === false) {
            echo $language['mc_err_connection'] , '<br />' , "\n";
            return;
        } else
            echo $language['mc_ok_connection'] , '<br />' , "\n";
    }
    
    if ($mc_db_exists === false) {
        // Create Database
        $db_database = mysql_query('create database ' , $mc_db_database);
        if ($db_database === false)
        {
            echo $language['mc_err_database'] , '<br />' , "\n";
            return;
        } else
            echo $language['mc_ok_database'] , '<br />' , "\n";
    }
    
    $db_change = mysql_query('use ' , $mc_db_database);
    
    // Create Table for the movies (with the autoincrementing id)
    $db_id = mysql_query('create table ' , $mc_db_table_movie , ' (id int not null auto_increment, index (id))');
    if ($db_id === false) {
        echo $language['mc_err_id'] , '<br />' , "\n";
        return;
    } else
        echo $language['mc_ok_id'] , '<br />' , "\n";
    
    // Create "movie" (varchar, max 100 chars)
    $db_movie = mysql_query('alter table ' , $mc_db_table_movie , ' add movie varchar(100) not null');
    if ($db_movie === false) {
        echo $language['mc_err_movie'] , '<br />' , "\n";
        return;
    } else
        echo $language['mc_ok_movie'] , '<br />' , "\n";
    
    // Create "genre" (varchar, max 20 chars)
    $db_genre = mysql_query('alter table ' , $mc_db_table_movie , ' add genre varchar(20)');
    if ($db_genre === false) {
        echo $language['mc_err_genre'] , '<br />' , "\n";
        return;
    } else
        echo $language['mc_ok_genre'] , '<br />' , "\n";
    
    // Create "genre2" (varchar, max 20 chars)
    if ($mc_genre2 === true) {
        $db_genre2 = mysql_query('alter table ' , $mc_db_table_movie , ' add genre2 varchar(20)');
        if ($db_genre2 === false) {
            echo $language['mc_err_genre2'] , '<br />' , "\n";
            return;
        } else
            echo $language['mc_ok_genre2'] , '<br />' , "\n";
    }
    
    // Create "description" (text, max text-field, about 64k chars)
    $db_desc = mysql_query('alter table ' , $mc_db_table_movie , ' add description text');
    if ($db_desc === false) {
        echo $language['mc_err_desc'] , '<br />' , "\n";
        return;
    } else
        echo $language['mc_ok_desc'] , '<br />' , "\n";
    
    
    // Create "release" (date)
    if ($mc_release === true) {
        $db_release = mysql_query('alter table ' , $mc_db_table_movie , ' add rel date');
        if ($db_release === false) {
            echo $language['mc_err_release'] , '<br />' , "\n";
            return;
        } else
            echo $language['mc_ok_release'] , '<br />' , "\n";
    }
    
    // Create "rating" (int)
    if ($mc_rating === true) {
        $db_rating = mysql_query('alter table ' , $mc_db_table_movie , ' add rating int');
        if ($db_rating === false) {
            echo $language['mc_err_rating'] , '<br />' , "\n";
            return;
        } else
            echo $language['mc_ok_rating'] , '<br />' , "\n";
    }
    
    // Create "medium" (varchar, 8 chars)
    if ($mc_medium === true) {
        $db_medium = mysql_query('alter table ' , $mc_db_table_movie , ' add medium char(8)');
        if ($db_medium === false){
            echo $language['mc_err_medium'] , '<br />' , "\n";
            return;
        } else
            echo $language['mc_ok_medium'] , '<br />' , "\n";
    }
    
    // Create "quantity" (int)
    if ($mc_quantity === true) {
        $db_quantity = mysql_query('alter table ' , $mc_db_table_movie , ' add quantity int');
        if ($db_quantity === false) {
            echo $language['mc_err_quantity'] , '<br />' , "\n";
            return;
        } else
            echo $language['mc_ok_quantity'] , '<br />' , "\n";
    }
    
    // Create "format" (varchar, 30 chars)
    if ($mc_format === true) {
        $db_format = mysql_query('alter table ' , $mc_db_table_movie , ' add format varchar(30)');
        if ($db_format === false) {
            echo $language['mc_err_format'] , '<br />' , "\n";
            return;
        } else
            echo $language['mc_ok_format'] , '<br />' , "\n";
    }
    
    // Create "place" (varchar, 20 chars)
    if ($mc_place === true) {
        $db_place = mysql_query('alter table ' , $mc_db_table_movie , ' add place varchar(20)');
        if ($db_place === false) {
            echo $language['mc_err_pplace'] , '<br />' , "\n";
            return;
        } else
            echo $language['mc_ok_pplace'] , '<br />' , "\n";
    }

    $user = mysql_query('CREATE TABLE ' , $mc_db_table_users , ' (userid int(2) not null auto_increment,
                         username varchar(30) NOT NULL, userpass text not null, userlevel int(1), index (userid))');

    echo $language['mc_ok_install'];
    mysql_close($db);

    echo '<form action="install_2.php" method="POST">' , "\n";
    echo $language['mc_username'] , '<input name="user" type="text" size="30" maxlength="30" /><br />' , "\n";
    echo $language['mc_password'] , '<input name="password" type="password" size="32" maxlength="32" /><p>' , "\n";
    echo '    <input type="submit" value="' , $language['mc_create'] , '" name="create" />' , "\n";
    echo '</form>' , "\n";
    }

    if (isset($_POST['create']) === true) {
        $db        = mysql_connect($mc_db_host, $mc_db_user, $mc_db_pass);
        $db_change = mysql_query('use ' , $mc_db_database);
        $username  = mysql_real_escape_string($_POST['user']);
        $passwort  = password_create($_POST['password']);
        $create    = mysql_query("insert into $mc_db_table_users set username='$username', userpass='$passwort', userlevel=3 ");
        echo '<br />' , "\n";
        if ($create === false)
            echo $language['mc_err_create_user'];
        else
            echo $language['mc_ok_create_user'];
        echo '<br />' , "\n";
    }
?>
</body>
</html>