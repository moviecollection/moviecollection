<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<meta http-equiv="pragma" content="no-cache">
<link rel="stylesheet" type="text/css" href="../settings/moviecollection.css">
<?php 
    /**
    @author Ruhland Matthias

    @date 16.05.2008
    @file install/install.php
    
    @brief Check all settings you made and then install it.
    
    @changes:
    08.01.2013    matthias    translated
    08.01.2013    matthias    charset ISO-8859-1 -> UTF-8
    */

    include '../settings/settings.php';     // File with settings
    include '../' , $mc_language;           // Include language
    
    echo '<title>' , $language['mc_title'] , '</title>' , "\n";
    echo '</head' , "\n";
    echo '<body>' , "\n";

    echo '<h1 id="title">' , $language['mc_installation'] , '</h1>' , "\n";

    echo '<div id="text">' , "\n";
    echo '<p>' , $language['mc_attention'] , '</p>' , "\n";

    echo '<p><strong>' , $language['mc_dbSettings'] , '</strong><br />' , "\n";
    echo $language['mc_host'] , ': <em>' , $mc_db_host , '</em><br />' , "\n";
    echo $language['mc_user'] , ': <em>' , $mc_db_user , '</em><br />' , "\n";
    echo $language['mc_password'] , ': <em>' , $mc_db_pass , '</em><br />' , "\n";
    echo $language['mc_db_exists'] , '? <em>';
    if ($mc_db_exists === true 'yes') echo $language['yes'];
    else                              echo $language['no'];
    echo '</em></p>' , "\n";

    echo '<p><strong>' , $language['mc_additional'] , '</strong><br />' , "\n";

    echo $language['mc_genre2'] , ': <em>';
    if ($mc_genre2 === true) echo $language['on'];
    else                           echo $language['off'];
    echo '</em><br />' , "\n";
    
    echo $language['mc_relese'] , ': <em>';
    if ($mc_release === true) echo $language['on'];
    else                            echo $language['off'];
    echo '</em><br />' , "\n";
    
    echo $language['mc_rating'] , ': <em>';
    if ($mc_rating === true) echo $language['on'];
    else                           echo $language['off'];
    echo '</em><br />' , "\n";
    
    echo $language['mc_medium'] , ': <em>';
    if ($mc_medium === true) echo $language['on'];
    else                           echo $language['off'];
    echo '</em><br />' , "\n";
    
    echo $language['mc_quantaty'] , ': <em>';
    if ($mc_quantity === true) echo $language['on'];
    else                             echo $language['off'];
    echo '</em><br />' , "\n";
    
    echo $language['mc_format'] , ': <em>';
    if ($mc_format === true) echo $language['on'];
    else                           echo $language['off'];
    echo '</em><br />' , "\n";
    
    echo $language['mc_place'] , ': <em>';
    if ($mc_place === true) echo $language['on'];
    else                          echo $language['off'];
    echo '</em><br />' , "\n";
    
    echo '</p>' , "\n";
    echo '<p>' , $language['mc_attention2'] , '</p>' , "\n";
    echo '<p>' , $language['mc_language'] , ': <em>' , $mc_language , '</em></p>' , "\n";
    echo '<p><strong><a href="install_2.php">' , $language['mc_create_db'] , '</a></strong></p>' , "\n";
    echo '</div>' , "\n";
?>
</body>
</html>