<?php
    /**
    @author Ruhland Matthias

    @date 27.05.2008
    @file userPassword.php
    
    @brief Page to change the own password
    */

    if (isset($_SESSION['userlevel']) === true) {
    
        if ($_SESSION['userlevel'] >= 1 && $_SESSION['ip'] === $_SERVER['REMOTE_ADDR']) {
            echo '<h1 id="title">' , $language['mc_changePassword'] , '</h1>' , "\n";
            
            if (isset($_POST['change']) === false) {
                echo '<form action="index.php?section=mc_pwChange" method="POST">' , "\n";
                echo $language['mc_passwordOld'] , '<input name="passwordOld" type="password" size="30" /><br />' , "\n";
                echo $language['mc_passwordNew'] , '<input name="passwordNew" type="password" size="30" /><br />' , "\n";
                echo $language['mc_passwordNew'] , '<input name="passwordNew2" type="password" size="30" /><br />' , "\n";
                echo '    <input type="submit" value="' , $language['mc_changePassword'] , '" name="change" /><br />' , "\n";
                echo '    <input type="reset" value="' , $language['mc_clear'] , '" />' , "\n";
                echo '</form>' , "\n";
            }

            if (isset($_POST['change']) === true) {
                include "password.inc.php";
                
                if ($_POST['passwordNew'] === $_POST['passwordNew2']) {
                    $db = mysql_connect($mc_db_host, $mc_db_user, $mc_db_pass);
                    if ($db === false) {
                        echo $language['mc_err_connection'] , '<br />' , "\n";
                        return;
                    }
                    
                    $db_change = mysql_query('use ' , $mc_db_database);
                    if ($db_change === false) {
                        echo $language['mc_use_database'] , '<br />' , "\n";
                        return;
                    }
                    
                    $username    = mysql_real_escape_string($_SESSION['user']);
                    $res = mysql_query("select * from $mc_db_table_users where username = '$username'");
                    if ($res === false) {
                        echo $language['mc_err_load_movie'] , '<br />' , "\n";
                        return;
                    }
                    
                    $newPwd      = password_create($_POST['passwordNew']);
                    $db_user     = mysql_result($res, 0, 'username');
                    $db_password = mysql_result($res, 0, 'userpass');
                    if ($newPwd === $db_password)
                        $language['mc_pw_identically'];
                    else {
                        if ($username === $db_user) {
                            $db_change = mysql_query('use ' , $mc_db_database);
                            $res       = "update $mc_db_table_users set userpass = '$newPwd' where username = '$username'";
                            $res       = mysql_query($res);
                            $num       = mysql_affected_rows($db);
                            echo '<p>' , ($num >= 1)?$language['mc_ok_pw_change']:echo $language['mc_err_pw_change'] , '</p>' , "\n";
                        }
                    }
                    mysql_close($db);
                }
                else
                    $language['mc_wrong_pw'];
            }
        }
        else
            echo $language['mc_not_loggedin'] , "\n";
    }
?>