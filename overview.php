<?php
    /**
    @author Ruhland Matthias

    @date 16.05.2008
    @file overview.php
    
    @brief Show a overview of all movies in the database
    
    Shows all movies in the database
    you can comment out some collumns
    for a little overview
    */

    if (isset($_SESSION['userlevel']) === true) {
    
        if ($_SESSION['userlevel'] >= 1 && $_SESSION['ip'] === $_SERVER['REMOTE_ADDR']) {
            echo '<h1 id="title">' , $language['mc_overview'] , '</h1>' , "\n";

            $db = mysql_connect($mc_db_host, $mc_db_user, $mc_db_pass);
            if ($db === false) {
                echo $language['mc_err_connection'] , '<br />' , "\n";
                return;
            }
            
            $db_change = mysql_query('use ' , $mc_db_database);
            if ($db_change === false) {
                echo $language['mc_use_database'] , '<br />' , "\n";
                return;
            }
            
            $res = mysql_query("select * from $mc_db_table_movie order by id asc");
            if ($res === false) {
                echo $language['mc_err_load_movie'] , '<br />' , "\n";
                return;
            }
            
            $num = mysql_num_rows($res);
            echo $num , ' ' , $language['mc_entries'] , '<br />' , "\n";

            echo '<table>' , "\n";
            echo '<tr>' , "\n";
            echo '<th>' , $language['mc_id'] , '</th>' , "\n";
            echo '<th>' , $language['mc_movie'] , '</th>' , "\n";
            echo '<th>' , $language['mc_genre'] , '</th>' , "\n";
            
            if ($mc_genre2 === true)
                echo '<th>' , $language['mc_genre2'] , '</th>' , "\n";
            
            echo '<th>' , $language['mc_description'] , '</th>' , "\n";
            
            if ($mc_release === true)
                echo '<th>' , $language['mc_relese'] , '</th>' , "\n";
            
            if ($mc_rating === true)
                echo '<th>' , $language['mc_rating'] , '</th>' , "\n";
            
            if ($mc_medium === true)
                echo '<th>' , $language['mc_medium'] , '</th>' , "\n";
            
            if ($mc_quantity === true)
                echo '<th>' , $language['mc_quantaty'] , '</th>' , "\n";
            
            if ($mc_format === true)
                echo '<th>' , $language['mc_format'] , '</th>' , "\n";
            
            if ($mc_place === true)
                echo '<th>' , $language['mc_place'] , '</th>' , "\n";
            
            echo '</tr>' , "\n";

            for ($i = 0; $i < $num; $i++) {
                $id    = mysql_result($res, $i, 'id');
                $movie = mysql_result($res, $i, 'movie');
                $genre = mysql_result($res, $i, 'genre');
                if ($mc_genre2 === true)
                    $genre2 = mysql_result($res, $i, 'genre2');
                
                $description = mysql_result($res, $i, 'description');
                if ($mc_release === true)
                    $release = mysql_result($res, $i, 'rel');
                
                if ($mc_rating === true)
                    $rating = mysql_result($res, $i, 'rating');
                
                if ($mc_medium === true)
                    $medium = mysql_result($res, $i, 'medium');
                
                if ($mc_quantity === true)
                    $quantity = mysql_result($res, $i, 'quantity');
                
                if ($mc_format === true)
                    $format = mysql_result($res, $i, 'format');
                
                if ($mc_place === true)
                    $place = mysql_result($res, $i, 'place');
                
                echo '<tr>' , "\n";
                echo '<td>' , $id , '</td>' , "\n";
                echo '<td>' , $movie , '</td>' , "\n";
                echo '<td>' , $genre , '</td>' , "\n";
                if ($mc_genre2 === true)
                    echo '<td>' , $genre2 , '</td>' , "\n";
                
                echo '<td>' , $description , '</td>' , "\n";
                if ($mc_release === true)
                    echo '<td>' , $release , '</td>' , "\n";
                
                if ($mc_rating === true)
                    echo '<td>' , $rating , '</td>' , "\n";
                
                if ($mc_medium === true)
                    echo '<td>' , $medium , '</td>' , "\n";
                
                if ($mc_quantity === true)
                    echo '<td>' , $quantity , '</td>' , "\n";
                
                if ($mc_format === true)
                    echo '<td>' , $format , '</td>' , "\n";
                
                if ($mc_place === true)
                    echo '<td>' , $place , '</td>' , "\n";
                
                echo '</tr>' , "\n";
            }
            echo '</table>' , "\n";
            
            mysql_close($db);
        }
        else
            echo $language['mc_not_loggedin'] , "\n";
        
    }
?>