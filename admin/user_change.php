<?php
    /**
    @author Ruhland Matthias

    @date 27.05.2008
    @file admin/user_change.php
    
    @brief Change all users
    */

    if (isset($_SESSION['userlevel']) === true) {
        
        if ($_SESSION['userlevel'] >= 3 && $_SESSION['ip'] == $_SERVER['REMOTE_ADDR']) {
            echo '<h1 id="title">' . $language['mc_user_change'] . '</h1>' . "\n";
            
            if (isset($_POST['load']) === false) {
                echo '<form action="index.php?section=mc_user_change" method="POST">' . "\n";
                echo '    ID: <input name="id" size="3" /><br />' . "\n";
                echo '    <input type="submit" value="' . $language['mc_user_load'] . '" name="load" />' . "\n";
                echo '    <input type="reset" value="' . $language['mc_clear'] . '" /><br />' . "\n";
                echo '</form>' . "\n";
            }

            if (isset($_POST['load']) === true) {
                $db  = mysql_connect($mc_db_host, $mc_db_user, $mc_db_pass);
                if ($db === false) {
                    echo $language['mc_err_connection'] , '<br />' , "\n";
                    return;
                }
                
                $db_change = mysql_query('use ' , $mc_db_database);
                if ($db_change === false) {
                    echo $language['mc_use_database'] , '<br />' , "\n";
                    return;
                }
                
                $id  = mysql_real_escape_string($_POST['id']);
                $res = mysql_query("select * from $mc_db_table_users where userid = $id");
                if ($res === false) {
                    echo $language['mc_err_load_user'] , '<br />' , "\n";
                    return;
                }
                
                $num = mysql_num_rows($res);
                echo $num . ' ' . $language['mc_entries'] . '<br />' . "\n";

                echo '<table>' . "\n";
                echo '<tr>' . "\n";
                echo '<th>' . $language['mc_user_id'] . '</th>' . "\n";
                echo '<th>' . $language['mc_user_name'] . '</th>' . "\n";
                echo '<th>' . $language['mc_user_level'] . '</th>' . "\n";
                echo '</tr>' . "\n";

                echo '<form action="index.php?section=mc_user_change" method="POST">' . "\n";

                for ($i = 0; $i < $num; $i++) {
                    $userid    = mysql_result($res, $i, "userid");
                    $username  = mysql_result($res, $i, "username");
                    $userlevel = mysql_result($res, $i, "userlevel");
                    echo '<tr>' . "\n";
                    echo '<td><input name="userid" size="2" value="' . $userid . '" readonly /></td>' . "\n";
                    echo '<td><input name="username" value="' . $username . '" maxlength="30" /></td>' . "\n";
                    echo '<td><select name="userlevel">' . "\n";
                    echo '        <option value="1"';
                    //if ($userlevel == '1') echo  ' selected ';
                    if ($userlevel === 1) echo  ' selected ';
                    echo '> ' . $language['mc_userlevel_1'] . '</option>' . "\n";
                    echo '        <option value="2"';
                    //if ($userlevel == '2') echo  ' selected ';
                    if ($userlevel === 2) echo  ' selected ';
                    echo '> ' . $language['mc_userlevel_2'] . '</option>' . "\n";
                    echo '        <option value="3"';
                    //if ($userlevel == '3') echo  ' selected ';
                    if ($userlevel === 3) echo  ' selected ';
                    echo '> ' . $language['mc_userlevel_3'] . '</option>' . "\n";
                    echo '</select></td>' . "\n";

                    echo '</tr>' . "\n";
                }

                mysql_close($db);
                echo '</table>' . "\n";
                echo '<input type="submit" value="' . $language['mc_user_change'] . '" name="change" />' . "\n";
                echo '<input type="reset" value="' . $language['mc_clear'] . '" /><br />' . "\n";
                echo '</form>' . "\n";
            }

            if (isset($_POST['change']) === true)
            {
                $db = mysql_connect($mc_db_host, $mc_db_user, $mc_db_pass);
                if ($db === false) {
                    echo $language['mc_err_connection'] , '<br />' , "\n";
                    return;
                }
                
                $db_change = mysql_query('use ' . $mc_db_database);
                if ($db_change === false) {
                    echo $language['mc_use_database'] , '<br />' , "\n";
                    return;
                }
                
                $change    = 'update ' . $mc_db_table_users . ' ';
                $username  = mysql_real_escape_string($_POST['username']);
                $change   .= "set username = '$username',";
                $userlevel = mysql_real_escape_string($_POST['userlevel']);
                $change   .= " userlevel = $userlevel";
                $userid    = mysql_real_escape_string($_POST['userid']);
                $change   .= " where userid = $userid";
                $update    = mysql_query($change);
                if ($update === false) {
                    echo $language['mc_user_err_changed'];
                    return;
                }
                
                $num = mysql_affected_rows();
                echo '<p>' , ($num >= 1)?$language['mc_user_ok_changed']:echo $language['mc_user_err_changed'] , '</p>' , "\n";
            }
        }
        else
            echo $language['mc_not_loggedin'] . "\n";
    }
?>