<?php
    /**
    @author Ruhland Matthias

    @date 27.05.2008
    @file admin/mc_user_delete.php
    
    @brief Delete a user from the database
    */

    if (isset($_SESSION['userlevel']) === true) {
    
        if ($_SESSION['userlevel'] >= 3 && $_SESSION['ip'] == $_SERVER['REMOTE_ADDR']) {
            echo '<h1 id="title">' . $language['mc_user_delete'] . '</h1>' . "\n";
            
            if (isset($_POST['load']) === false) {
                echo '<form action="index.php?section=mc_user_delete" method="POST">' . "\n";
                echo '    ID: <input name="id" size="3" /><br />' . "\n";
                echo '    <input type="submit" value="' . $language['mc_user_load'] . '" name="load" />' . "\n";
                echo '    <input type="reset" value="' . $language['mc_clear'] . '" /><br />' . "\n";
                echo '</form>' . "\n";
            }

            if (isset($_POST['load']) === true) {
                $db = mysql_connect($mc_db_host, $mc_db_user, $mc_db_pass);
                if ($db === false) {
                    echo $language['mc_err_connection'] , '<br />' , "\n";
                    return;
                }
                
                $db_change = mysql_query('use ' , $mc_db_database);
                if ($db_change === false) {
                    echo $language['mc_use_database'] , '<br />' , "\n";
                    return;
                }
                
                $id  = mysql_real_escape_string($_POST['id']);
                $res = mysql_query("select * from $mc_db_table_users where userid = $id");
                if ($res === false) {
                    echo $language['mc_err_load_user'] , '<br />' , "\n";
                    return;
                }
                
                $num = mysql_num_rows($res);
                echo $num . ' ' . $language['mc_entries'] . '<br />' . "\n";
                
                echo '<table>' . "\n";
                echo '<tr>' . "\n";
                echo '<th>' . $language['mc_user_id'] . '</th>' . "\n";
                echo '<th>' . $language['mc_user_name'] . '</th>' . "\n";
                echo '<th>' . $language['mc_user_level'] . '</th>' . "\n";
                echo '</tr>' . "\n";

                echo '<form action="index.php?section=mc_user_delete" method="POST">' . "\n";

                for ($i = 0; $i < $num; $i++) {
                    $userid    = mysql_result($res, $i, 'userid');
                    $username  = mysql_result($res, $i, 'username');
                    $userlevel = mysql_result($res, $i, 'userlevel');
                    if ($userlevel === 1)
                        $userlevel = $language['mc_userlevel_1'];
                    if ($userlevel === 2)
                        $userlevel = $language['mc_user_delete'];
                    if ($userlevel === 3)
                        $userlevel = $language['mc_user_delete'];
                    
                    echo '<tr>' . "\n";
                    echo '<input name="userid" type="hidden" value="' . $userid . '" />' . "\n";
                    echo '<td>' . $userid . '</td>' . "\n";
                    echo '<td>' . $username . '</td>' . "\n";
                    echo '<td>' . $userlevel . '</td>' . "\n";
                    echo '</tr>' . "\n";
                    echo '<tr>' . "\n";
                }
                mysql_close($db);
                echo '</table>' . "\n";
                echo '<input type="submit" value="' . $language['mc_user_delete'] . '" name="delete" />' . "\n";
                echo '<input type="reset" value="' . $language['mc_clear'] . '" /><br />' . "\n";
                echo '</form>' . "\n";
            }

            if (isset($_POST['delete']) === true) {
                $db = mysql_connect($mc_db_host, $mc_db_user, $mc_db_pass);
                if ($db === false) {
                    echo $language['mc_err_connection'] , '<br />' , "\n";
                    return;
                }
                
                $db_change = mysql_query('use ' . $mc_db_database);
                if ($db_change === false) {
                    echo $language['mc_use_database'] , '<br />' , "\n";
                    return;
                }
                
                $delete  = 'delete from ' . $mc_db_table_users . ' ';
                $userid  = mysql_real_escape_string($_POST['userid']);
                $delete .= " where userid = $userid";
                $exec    = mysql_query($delete);
                if ($exec === false) {
                    echo $language['mc_user_err_deleted'];
                    return;
                }
                
                $num = mysql_affected_rows();
                echo '<p>' , ($num >= 1)?$language['mc_user_ok_deleted']:echo $language['mc_user_err_deleted'] , '</p>' , "\n";
            }
        }
        else
            echo $language['mc_not_loggedin'] . "\n";
    }
?>