<?php
    /**
    @author Ruhland Matthias

    @date 27.05.2008
    @file admin/user_add.php
    
    @brief Add a user
    */

    if (isset($_SESSION['userlevel']) === true) {
        if ($_SESSION['userlevel'] >= 3 && $_SESSION['ip'] === $_SERVER['REMOTE_ADDR']) {
            echo '<h1 id="title">' . $language['mc_add_user'] . '</h1>' . "\n";
            
            if (isset($_POST['add']) === false) {
                echo '<form action="index.php?section=mc_user_add" method="POST">' . "\n";
                echo $language['mc_username'] . '<input name="user" size="30" maxlength="30" /><br />' . "\n";
                echo $language['mc_password'] . '<input name="password1" type="password" size="30" /><br />' . "\n";
                echo $language['mc_password'] . '<input name="password2" type="password" size="30" /><br />' . "\n";
                echo $language['fs_userlevel'];
                echo '    <select name="userlevel">' . "\n";
                echo '        <option value="1" selected>' . $language['mc_userlevel_1'] . '</option>' . "\n";
                echo '        <option value=\"2\">' . $language['mc_userlevel_2'] . '</option>' . "\n";
                echo '        <option value=\"3\">' . $language['mc_userlevel_3'] . '</option>' . "\n";
                echo '    </select><br />' . "\n";
                echo '    <input type="submit" value="' . $language['mc_add_user'] . '" name="add" />' . "\n";
                echo '    <input type="reset" value="' . $language['mc_clear'] . '" />' . "\n";
                echo '</form>' . "\n";
            }

            if (isset($_POST['add']) === true) {
                if ($_POST['password1'] === $_POST['password2']) {
                    include 'password.inc.php';
                    
                    $db = mysql_connect($mc_db_host, $mc_db_user, $mc_db_pass);
                    if ($db === false) {
                        echo $language['mc_err_connection'] , '<br />' , "\n";
                        return;
                    }
                    
                    $db_change = mysql_query('use ' , $mc_db_database);
                    if ($db_change === false) {
                        echo $language['mc_use_database'] , '<br />' , "\n";
                        return;
                    }
                
                    $send      = 'insert into ' . $mc_db_table_users . ' (';
                    $username  = mysql_real_escape_string($_POST['user']);
                    $passwort  = password_create($_POST['password']);
                    $userlevel = mysql_real_escape_string($_POST['userlevel']);
                    $send .= 'username, userpass, userlevel) values (';
                    $send .= "'$username', '$passwort', '$userlevel')";
                    $inserted = mysql_query($send);
                    if ($inserted === false) {
                        echo $language['mc_err_useradd'];
                        return;
                    }
                    
                    $num = mysql_affected_rows();
                    echo '<p>' , ($num >= 1)?$language['mc_ok_useradd']:echo $language['mc_err_useradd'] , '</p>' , "\n";
                    mysql_close($db);
                }
                else
                    echo $language['mc_wrong_pw'] . '<br />' . "\n";
            }
        }
        else
            echo $language['mc_not_loggedin'] . "\n";
    }
?>