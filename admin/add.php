<?php
    /**
    @author Ruhland Matthias

    @date 27.05.2008
    @file admin/add.php
    
    @brief Add new users to the database
    */

    if (isset($_SESSION['userlevel']) === true) {
        if ($_SESSION['userlevel'] >= 2 && $_SESSION['ip'] === $_SERVER['REMOTE_ADDR']) {
            echo '<h1 id="title">' , $language['mc_add'] , '</h1>' , "\n";

            if (isset($_POST['sent']) === true) {
                $db = mysql_connect($mc_db_host, $mc_db_user, $mc_db_pass);
                if ($db === false) {
                    echo $language['mc_err_connection'] , '<br />' , "\n";
                    return;
                }
                
                $send  = 'insert into ' , $mc_db_table_movie , ' ';
                $send .= '(movie, genre';
                if ($mc_genre2 === true)
                    $send .= ', genre2';
                
                $send .= ', description';
                if ($mc_release === true)
                    $send .= ', rel';
                
                if ($mc_rating === true)
                    $send .= ', rating';
                
                if ($mc_medium === true)
                    $send .= ', medium';
                
                if ($mc_quantity === true)
                    $send .= ', quantity';
                
                if ($mc_format === true)
                    $send .= ', format';
                
                if ($mc_place === true)
                    $send .= ', place';
                
                $send .= ') values (';
                $movie = mysql_real_escape_string($_POST['movie']);
                $send .= "'$movie'";
                $genre = mysql_real_escape_string($_POST['genre']);
                $send .= ", '$genre'";
                if ($mc_genre2 === true) {
                    $genre2 = mysql_real_escape_string($_POST['genre2']);
                    $send .= ", '$genre2'";
                }
                $description = mysql_real_escape_string($_POST['description']);
                $send .= ", '$description'";
                if ($mc_release === true) {
                    $release = mysql_real_escape_string($_POST['release']);
                    $send .= ", '$release'";
                }
                if ($mc_rating === true) {
                    $rating = mysql_real_escape_string($_POST['rating']);
                    $send .= ", $rating";
                }
                if ($mc_medium === true) {
                    $medium = mysql_real_escape_string($_POST['medium']);
                    $send .= ", '$medium'";
                }
                if ($mc_quantity === true) {
                    $quantity = mysql_real_escape_string($_POST['quantity']);
                    $send .= ", $quantity";
                }
                if ($mc_format === true) {
                    $format = mysql_real_escape_string($_POST['format']);
                    $send .= ", '$format'";
                }
                if ($mc_place === true) {
                    $place = mysql_real_escape_string($_POST['place']);
                    $send .= ", '$place'";
                }
                $send .= ')';
                $db_change = mysql_query('use ' , $mc_db_database);
                if ($db_change === false) {
                    echo $language['mc_use_database'] , '<br />' , "\n";
                    return;
                }
                
                $insert = mysql_query($send);
                if ($insert === false) {
                    echo $language['mc_err_add_movie'] , '<br />' , "\n";
                    return;
                }
                
                $num = mysql_affected_rows();
                echo '<p>' , ($num >= 1)?$language['mc_ok_data_add']:echo $language['mc_err_add_movie'] , '</p>' , "\n";
                mysql_close($db);
            }

            echo '<form action="index.php?section=mc_add" method="POST">' , "\n";
            echo $language['mc_movie'] , ': <input name="movie" size="30" maxlength="100" /> ' , $language['mc_data_max_100'] , '<br />' , "\n";
            echo $language['mc_genre'] , ': <input name="genre" size="20" maxlength="20" /> ' , $language['mc_data_max_20'] , '<br />' , "\n";
            if ($mc_genre2 === true)
                echo $language['mc_genre2'] , ': <input name="genre2" size="20" maxlength="20" /> ' , $language['mc_data_max_20'] , '<br />' , "\n";
            
            echo $language['mc_description'] , ': ' , $language['mc_data_max_text'] , '<br />' , "\n";
            echo '    <textarea cols="100" rows="9" name="description"> </textarea><p>' , "\n"; // 65536 Zeichen
            if ($mc_release === true)
                echo $language['mc_relese'] , ': <input name="release" size="10" maxlength="10" /> ' , $language['mc_data_max_date'] , '<br />' , "\n";
            
            if ($mc_rating === true)
                echo $language['mc_rating'] , ': <input name="rating" size="2" maxlength="2" /> ' , $language['mc_data_max_2'] , '<br />' , "\n";
            
            if ($mc_medium === true) {
                echo $language['mc_medium'] , ': ' , "\n";
                echo '    <select name="medium">' , "\n";
                echo '        <option value="CD"> ' , $language['mc_medium_cd'] , '</option>' , "\n";
                echo '        <option value="DVD" selected> ' , $language['mc_medium_dvd'] , '</option>' , "\n";
                echo '        <option value="Blue-Ray"> ' , $language['mc_medium_blue'] , '</option>' , "\n";
                echo '        <option value="VHS"> ' , $language['mc_medium_vhs'] , '</option>' , "\n";
                echo '    </select><br />' , "\n";
            }
            if ($mc_quantity === true)
                echo $language['mc_quantaty'] , ': <input name="quantity" size="1" maxlength="1" /> ' , $language['mc_data_max_1'] , '<br />' , "\n";
            
            if ($mc_format === true)
                echo $language['mc_format'] , ': <input name="format" size="30" maxlength="30" /> ' , $language['mc_data_max_30'] , '<br />' , "\n";
            
            if ($mc_place === true)
                echo $language['mc_place'] , ': <input name="place" size="20" maxlength="20" /> ' , $language['mc_data_max_20'] , '<br />' , "\n";
            
            echo '    <input type="submit" value="' , $language['mc_data_send'] , '" name="sent" />' , "\n";
            echo '    <input type="reset" value="' , $language['mc_clear'] , '" />' , "\n";
            echo '</form>' , "\n";
        }
        else
            echo $language['mc_not_loggedin'] , "\n";
    }
?>