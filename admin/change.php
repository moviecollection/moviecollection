<?php
    /**
    @author Ruhland Matthias

    @date 27.05.2008
    @file change.php
    
    @brief Change a movie-entrie in the database
    */

    if (isset($_SESSION['userlevel']) === true) {
        if ($_SESSION['userlevel'] >= 2 && $_SESSION['ip'] === $_SERVER['REMOTE_ADDR']) {
            echo '<h1 id="title">' , $language['mc_change'] , '</h1>' , "\n";

            if (isset($_POST['load']) === false) {
                echo '<form action="index.php?section=mc_change" method="POST">' , "\n";
                echo '    ID: <input name="id" size="3" /><br />' , "\n";
                echo '    <input type="submit" value="' , $language['mc_load_movie'] , '" name="load" />' , "\n";
                echo '    <input type="reset" value="' , $language['mc_clear'] , '" /><br />' , "\n";
                echo '</form>' , "\n";
            }

            // Load movie
            if (isset($_POST['load'])) {
                $db = mysql_connect($mc_db_host, $mc_db_user, $mc_db_pass);
                if ($db === false) {
                    echo $language['mc_err_connection'] , '<br />' , "\n";
                    return;
                }
                
                $db_change = mysql_query('use ' , $mc_db_database);
                if ($db_change === false) {
                    echo $language['mc_use_database'] , '<br />' , "\n";
                    return;
                }
                
                $id = mysql_real_escape_string($_POST['id']);
                
                $res = mysql_query("select * from $mc_db_table_movie where id = '$id'");
                if ($res === false) {
                    echo $language['mc_err_load_movie'] , '<br />' , "\n";
                    return;
                }
                
                $num = mysql_num_rows($res);
                echo $num , ' ' , $language['mc_entries'] , '<br />' , "\n";
                
                echo '<table>' , "\n";
                echo '<tr>' , "\n";
                echo '<th>' , $language['mc_id'] , '</th>' , "\n";
                echo '<th>' , $language['mc_movie'] , '</th>' , "\n";
                echo '<th>' , $language['mc_genre'] , '</th>' , "\n";
                if ($mc_genre2 === true)
                    echo '<th>' , $language['mc_genre2'] , '</th>' , "\n";
                
                echo '<th>' , $language['mc_description'] , '</th>' , "\n";
                if ($mc_release === true)
                    echo '<th>' , $language['mc_relese'] , '</th>' , "\n";
                
                if ($mc_rating === true)
                    echo '<th>' , $language['mc_rating'] , '</th>' , "\n";
                
                if ($mc_medium === true)
                    echo '<th>' , $language['mc_medium'] , '</th>' , "\n";
                
                if ($mc_quantity === true)
                    echo '<th>' , $language['mc_quantaty'] , '</th>' , "\n";
                
                if ($mc_format === true)
                    echo '<th>' , $language['mc_format'] , '</th>' , "\n";
                
                if ($mc_place === true)
                    echo '<th>' , $language['mc_place'] , '</th>' , "\n";
                
                echo '</tr>' , "\n";

                echo '<form action="index.php?section=mc_change" method="POST">' , "\n";

                for ($i = 0; $i < $num; $i++) {
                    $id    = mysql_result($res, $i, 'id');
                    $movie = mysql_result($res, $i, 'movie');
                    $genre = mysql_result($res, $i, 'genre');
                    if ($mc_genre2 === true)
                        $genre2 = mysql_result($res, $i, 'genre2');
                    
                    $description = mysql_result($res, $i, 'description');
                    if ($mc_release === true)
                        $release = mysql_result($res, $i, 'rel');
                    
                    if ($mc_rating === true)
                        $rating = mysql_result($res, $i, 'rating');
                    
                    if ($mc_medium === true)
                        $medium = mysql_result($res, $i, 'medium');
                    
                    if ($mc_quantity === true)
                        $quantity = mysql_result($res, $i, 'quantity');
                    
                    if ($mc_format === true)
                        $format = mysql_result($res, $i, 'format');
                    
                    if ($mc_place === true)
                        $place = mysql_result($res, $i, 'place');
                
                    echo '<tr>' , "\n";
                    echo '<td><input name="id" size="2" value="' , $id , '" readonly /></td>' , "\n";
                    echo '<td><input name="movie" value="' , $movie , '" maxlength="100" /></td>' , "\n";
                    echo '<td><input name="genre" value="' , $genre , '" maxlength="20" /></td>' , "\n";
                    if ($mc_genre2 === true)
                        echo '<td><input name="genre2" value="' , $genre2 , '" maxlength="20" /></td>' , "\n";
                    
                    echo '<td><input name="description" value="' , $description , '" /></td>' , "\n";
                    if ($mc_release === true)
                        echo '<td><input name="release" size="10" value="' , $release , '" /></td>' , "\n";
                    
                    if ($mc_rating === true)
                        echo '<td><input name="rating" size="2" value="' , $rating , '" /></td>' , "\n";
                    
                    if ($mc_medium === true) {
                        echo '<td><select name="medium">' , "\n";
                        echo '        <option value="CD"';
                        if ($medium === 'CD') echo  ' selected ';
                        echo '> ' , $language['mc_medium_cd'] , '</option>' , "\n";
                        echo '        <option value="DVD"';
                        if ($medium === 'DVD') echo  ' selected ';
                        echo '> ' , $language['mc_medium_dvd'] , '</option>' , "\n";
                        echo '        <option value="Blue-Ray"';
                        if ($medium === 'Blue-Ray') echo  ' selected ';
                        echo '> ' , $language['mc_medium_blue'] , '</option>' , "\n";
                        echo '        <option value="VHS"';
                        if ($medium === 'VHS') echo  ' selected ';
                        echo '> ' , $language['mc_medium_vhs'] , '</option>' , "\n";
                        echo '</select></td>' , "\n";
                    }
                    
                    if ($mc_quantity === true)
                        echo '<td><input name="quantity" size="1" value="' , $quantity , '" /></td>' , "\n";
                    
                    if ($mc_format === true)
                        echo '<td><input name="format" value="' , $format , '" maxlength="30" /></td>' , "\n";
                    
                    if ($mc_place === true) 
                        echo '<td><input name="place" value="' , $place , '" maxlength="20" /></td>' , "\n";
                    
                    echo '</tr>' , "\n";
                    echo '<tr>' , "\n";
                    echo '<td> </td>' , "\n";
                    echo '<td>' , $language['mc_data_max_100'] , '</td>' , "\n";
                    echo '<td>' , $language['mc_data_max_20'] , '</td>' , "\n";
                    if ($mc_genre2 === true)
                        echo '<td> ' , $language['mc_data_max_20'] , '</td>' , "\n";
                    
                    echo '<td> ' , $language['mc_data_max_text'] , '</td>' , "\n";
                    if ($mc_release === true)
                        echo '<td> ' , $language['mc_data_max_date'] , '</td>' , "\n";
                    
                    if ($mc_rating === true)
                        echo '<td> ' , $language['mc_data_max_2'] , '</td>' , "\n";
                    
                    if ($mc_medium === true)
                        echo '<td></td>' , "\n";
                        //echo '<td> ' , $language['mc_data_max_3'] , '</td>' , "\n";
                    
                    if ($mc_quantity === true)
                        echo '<td> ' , $language['mc_data_max_1'] , '</td>' , "\n";
                    
                    if ($mc_format === true)
                        echo '<td> ' , $language['mc_data_max_30'] , '</td>' , "\n";
                    
                    if ($mc_place === true)
                        echo '<td> ' , $language['mc_data_max_20'] , '</td>' , "\n";
                    
                    echo '</tr>' , "\n";
                }
                mysql_close($db);
                echo '</table>' , "\n";
                echo '<input type="submit" value="' , $language['mc_change_movie'] , '" name="change" />' , "\n";
                echo '<input type="reset" value="' , $language['mc_clear'] , '" /><br />' , "\n";
                echo '</form>' , "\n";
            }

            if (isset($_POST['change']) === true) {
                $db  = mysql_connect($mc_db_host, $mc_db_user, $mc_db_pass);
                if ($db === false) {
                    echo $language['mc_err_connection'] , '<br />' , "\n";
                    return;
                }
                
                $db_change = mysql_query('use ' , $mc_db_database);
                if ($db_change === false) {
                    echo $language['mc_use_database'] , '<br />' , "\n";
                    return;
                }
                
                $change  = 'update ' , $mc_db_table_movie , ' ';
                $movie   = mysql_real_escape_string($_POST['film']);
                $change .= "set movie = '$movie'";
                $genre   = mysql_real_escape_string($_POST['genre']);
                $change .= ", genre = '$genre'";
                if ($mc_genre2 === true)
                {
                    $genre2 = mysql_real_escape_string($_POST['genre2']);
                    $change .= ", genre2 = '$genre2'";
                }
                $description = mysql_real_escape_string($_POST['description']);
                $change .= ", description = '$description'";
                if ($mc_release === true)
                {
                    $release = mysql_real_escape_string($_POST['release']);
                    $change .= ", release = '$release'";
                }
                if ($mc_rating === true)
                {
                    $rating = mysql_real_escape_string($_POST['rating']);
                    $change .= ", rating = '$rating'";
                }
                if ($mc_medium === true)
                {
                    $medium = mysql_real_escape_string($_POST['medium']);
                    $change .= ", medium = '$medium'";
                }
                if ($mc_quantity === true)
                {
                    $quantity = mysql_real_escape_string($_POST['quantity']);
                    $change .= ", quantity = '$quantity'";
                }
                if ($mc_format === true)
                {
                    $format = mysql_real_escape_string($_POST['format']);
                    $change .= ", format = '$format'";
                }
                if ($mc_place === true)
                {
                    $place = mysql_real_escape_string($_POST['place']);
                    $change .= ", place = '$place'";
                }
                $id = mysql_real_escape_string($_POST['id']);
                $change .= " where id = $id";
                $changeing = mysql_query($change);
                if ($changing === false) {
                    echo $language['mc_err_change_movie'];
                    return;
                }
                
                $num = mysql_affected_rows();
                echo '<p>' , ($num >= 1)?$language['mc_changed_movie']:echo $language['mc_err_change_movie'] , '</p>' , "\n";
            }
        }
        else
            echo $language['mc_not_loggedin'] , "\n";
    }
?>