<?php
    /**
    @author Ruhland Matthias

    @date 27.05.2008
    @file admin/user_overview.php
    
    @brief Shows all users
    */

    if (isset($_SESSION['userlevel']) === true) {
    
        if ($_SESSION['userlevel'] >= 3 && $_SESSION['ip'] === $_SERVER['REMOTE_ADDR']) {
            echo '<h1 id="title">' , $language['mc_user_overview'] , '</h1>' , "\n";
            
            $db  = mysql_connect($mc_db_host, $mc_db_user, $mc_db_pass);
            if ($db === false) {
                echo $language['mc_err_connection'] , '<br />' , "\n";
                return;
            }
            
            $db_change = mysql_query('use ' , $mc_db_database);
            if ($db_change === false) {
                echo $language['mc_use_database'] , '<br />' , "\n";
                return;
            }

            $res = mysql_db_query($mc_db_database, "select * from $mc_db_table_users order by userid asc");
            if ($res === false) {
                echo $language['mc_err_load_user'] , '<br />' , "\n";
                return;
            }
            
            $num = mysql_num_rows($res);
            echo $num , ' ' , $language['mc_entries'] , '<br />' , "\n";

            echo '<table>' , "\n";
            echo '<tr>' , "\n";
            echo '<th>' , $language['mc_user_id'] , '</th>' , "\n";
            echo '<th>' , $language['mc_user_name'] , '</th>' , "\n";
            echo '<th>' , $language['mc_user_level'] , '</th>' , "\n";
            echo '</tr>' , "\n";

            for ($i = 0; $i < $num; $i++) {
                $userid    = mysql_result($res, $i, 'userid');
                $username  = mysql_result($res, $i, 'username');
                $userlevel = mysql_result($res, $i, 'userlevel');
                echo '<tr>' , "\n";
                echo '<td>' , $userid , '</td>' , "\n";
                echo '<td>' , $username , '</td>' , "\n";
                     if ($userlevel === 1) echo '<td>' , $language['mc_userlevel_1'] , '</td>' , "\n";
                else if ($userlevel === 2) echo '<td>' , $language['mc_userlevel_2'] , '</td>' , "\n";
                else if ($userlevel === 3) echo '<td>' , $language['mc_userlevel_3'] , '</td>' , "\n";
                //     if ($userlevel == '1') echo '<td>' , $language['mc_userlevel_1'] , '</td>' , "\n";
                //else if ($userlevel == '2') echo '<td>' , $language['mc_userlevel_2'] , '</td>' , "\n";
                //else if ($userlevel == '3') echo '<td>' , $language['mc_userlevel_3'] , '</td>' , "\n";
                echo '</tr>' , "\n";
            }
            echo '</table>' , "\n";
            mysql_close($db);
        }
        else
            echo $language['mc_not_loggedin'] , "\n";
    }
?>