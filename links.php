<?php
    /**
    @author Ruhland Matthias

    @date 16.05.2008
    @file links.php
    
    Array with all links in the movie collection
    */

    $links = array(
    'mc_login'          => 'login.php',
    'mc_logout'         => 'logout.php',
    'mc_mainpage'       => 'home.html',
    'mc_all'            => 'all.php',
    'mc_overview'       => 'overview.php',
    'mc_search'         => 'search.php',
    'mc_add'            => 'admin/add.php',
    'mc_change'         => 'admin/change.php',
    'mc_delete'         => 'admin/delete.php',
    'mc_pwChange'       => 'userPassword.php',
    'mc_user_overview'  => 'admin/user_overview.php',
    'mc_user_add'       => 'admin/user_add.php',
    'mc_user_change'    => 'admin/user_change.php',
    'mc_user_delete'    => 'admin/user_delete.php'
    );
?>