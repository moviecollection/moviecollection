<?php
    /**
    @author Ruhland Matthias

    @date 16.05.2008
    @file deutsch.php
    
    German language file
    
    @changes:
    08.01.2013    matthias    translated
    08.01.2013    matthias    charset ISO-8859-1 -> UTF-8
    */

    $language = array(
    // Header
    'mc_title'                => 'Filmsammlung by JaRuhl',

    // Installation
    

    // index.php
    'mc_menu_login'         => 'Login',
    'mc_menu_logout'        => 'Logout',
    'mc_menu_mainpage'      => 'Startseite',
    'mc_menu_overview'      => 'Übersicht',
    'mc_menu_allFilms'      => 'Alle Filme',
    'mc_menu_searchFilm'    => 'Film suchen',

    'mc_menu_add'           => 'Film eintragen',
    'mc_menu_change'        => 'Film ändern',
    'mc_menu_delete'        => 'Film löschen',

    'mc_menu_pwd_change'    => 'Passwort ändern',
    'mc_menu_user_overview' => 'Benutzerübersicht',
    'mc_mneu_user_add'      => 'Benutzer hinzufügen',
    'mc_menu_user_change'   => 'Benutzer ändern',
    'mc_menu_user_delete'   => 'Benutzer löschen',

    // all.php
    'mc_all'                => 'Filmsammlung - Alle Filme',
    'mc_entries'            => 'Datensätze gefunden',
    'mc_id'                 => 'Film-ID',
    'mc_movie'              => 'Film',
    'mc_genre'              => 'Genre',
    'mc_genre2'             => 'Genre 2',
    'mc_description'        => 'Beschreibung',
    'mc_relese'             => 'Veröffentlichung',
    'mc_rating'             => 'FSK',
    'mc_medium'             => 'Medium',
    'mc_quantaty'           => 'Anzahl',
    'mc_format'             => 'Format',
    'mc_place'              => 'Ort',
    // overview.php
    'mc_overview'           => 'Filmsammlung - Übersicht',

    // search.php
    'mc_search'             => 'Filmsammlung - Film suchen',
    'mc_search_for'         => 'Suche nach (Bitte anklicken):',
    'mc_search_movie'       => 'Film suchen',

    // add.php
    'mc_add'                => 'Filmsammlung - Film eintragen',
    'mc_data_send'          => 'Film speichern',
    'mc_ok_data_add'        => 'Es wurde ein Datensatz hinzugefügt',
    'mc_err_data_add'       => 'Es ist ein Fehler aufgetreten. Es wurde kein Datensatz hinzugefügt',
    'mc_data_max_100'       => 'max: 100 Zeichen',
    'mc_data_max_30'        => 'max: 30 Zeichen',
    'mc_data_max_20'        => 'max: 20 Zeichen',
    'mc_data_max_3'         => 'max: 3 Zeichen',
    'mc_data_max_2'         => 'max: 2 Zeichen',
    'mc_data_max_1'         => 'max: 1 Zahl',
    'mc_medium_dvd'         => 'DVD',
    'mc_medium_cd'          => 'CD',
    'mc_medium_blue'        => 'Blue-Ray',
    'mc_medium_vhs'         => 'VHS',
    'mc_data_max_text'      => 'max: 65536 Zeichen',
    'mc_data_max_date'      => 'Datum in der Form 2008-05-27',
    'mc_err_add_movie'      => 'FEHLER: Konnte Film nicht hinzufügen',

    // change.php
    'mc_change'             => 'Filmsammlung - Film ändern',
    'mc_load_movie'         => 'Film laden',
    'mc_change_movie'       => 'Film ändern',
    'mc_changed_movie'      => 'Film geändert',
    'mc_err_change_movie'   => 'FEHLER: Film konnte nicht geändert werden!',
    'mc_err_load_movie'     => 'FEHLER: Konnte Filem nicht finden',

    // delete.php
    'mc_delete_movie'       => 'Filmsammlung - Film löschen',
    'mc_movie_delete'       => 'Film löschen',
    'mc_movie_deleted'      => 'Film gelöscht',
    'mc_err_movie_delete'   => 'FEHLER: Film konnte nicht gelöscht werden!',
    
    // login.php
    'mc_loggingin'          => 'Anmelden',
    'mc_clear'              => 'Verwerfen',
    'mc_err_user'           => 'FEHLER: Benutzername oder Passwort stimmen nicht überein',
    'mc_not_loggedin'       => 'FEHLER: Sie sind nicht eingeloggt! Die Seite ist nicht gültig!',

    // logout.php
    'mc_loggedout'          => 'Sie wurden erfolgreich ausgeloggt!',
    'mc_reload1'            => 'Seite mit F5 neu aufbauen oder ',
    'mc_reload2'            => 'hier',
    'mc_reload3'            => ' klicken',

    // userPassword.php
    'mc_passwordOld'        => 'Altes Passwort: ',
    'mc_passwordNew'        => 'Neues Passwort: ',
    'mc_changePassword'     => 'Passwort ändern',
    'mc_wrong_pw'           => 'Passwörter stimmen nicht überein',
    'mc_pw_identically'     => 'Altes und neues Passwort sind identisch<br />\n
        Es gibt nichts zu updaten',
    'mc_ok_pw_change'       => 'Passwort wurde geändert',
    'mc_err_pw_change'      => 'FEHLER: Passwort konnte nicht geändert werden',

    // user_add.php
    'mc_add_user'           => 'Benutzer hinzufügen',
    'mc_userlevel_1'        => 'Filme sehen',
    'mc_userlevel_2'        => 'Filme verwalten',
    'mc_userlevel_3'        => 'Benutzer verwalten',
    'mc_ok_useradd'         => 'Benutzer wurde hinzugefügt',
    'mc_err_useradd'        => 'FEHLER: Benutzer konnte nicht hinzugefügt werden',

    // user_overview.php
    'mc_user_overview'      => 'Benutzerübersicht',
    'mc_user_id'            => 'Benutzerid',
    'mc_user_name'          => 'Benutzername',
    'mc_user_level'         => 'Benutzerlevel',

    // user_change.php
    'mc_user_change'        => 'Benutzer ändern',
    'mc_user_load'          => 'Benutzer laden',
    'mc_user_ok_changed'    => 'Benutzer wurde erfolgreich geändert',
    'mc_user_err_changed'   => 'FEHLER: Benutzer konnet nicht geändert werden',
    'mc_err_load_user'      => 'FEHLER: Konnte Benutzer nicht finden',

    // mc_user_delete.php
    'mc_user_delete'        => 'Benutzer löschen',
    'mc_user_ok_deleted'    => 'Benutzer gelöscht',
    'mc_user_err_deleted'   => 'FEHLER: Benutzer konnte nicht gelöscht werden',
    
    // install.php
    'mc_installation'       => 'Installation',
    'mc_attention'          => 'Bitte überprüfen Sie folgende Angaben und passen Sie gegebenfalls die Datei settings/settings.php an.
        Spätere Änderungen sind nur mit viel Aufwand realisierbar!',
    'mc_dbSettings'         => 'Datenbank-Settings:',
    'mc_host'               => 'Host',
    'mc_user'               => 'Benutzer',
    'mc_password'           => 'Passwort',
    'mc_db_exists'          => 'Datenbank vorhanden',
    'yes'                   => 'Ja',
    'no'                    => 'Nein',
    'mc_additional'         => 'Zusätze',
    'on'                    => 'ein',
    'off'                   => 'aus',

    'mc_attention2'         => 'Diese weiteren Änderungen können später leichter geändert werden.
        Dennoch sollten diese überprüft werden.',
    'mc_language'           => 'Sprache',

    'mc_create_db'          => 'Datenbank erzeugen',
    'mc_ok_connection'      => 'Verbindung zur Datenbank hergestellt',
    'mc_err_connection'     => 'FEHLER: Es konnte keine Verbindung zu Datenbank hergestellt werden',
    'mc_err_database'       => 'FEHLER: Datenbank konnte nicht erstellt werden',
    'mc_ok_database'        => 'Datenbank erstellt',
    'mc_err_id'             => 'FEHLER: Tabelle konnte nicht erstellt werden, oder id konnte nicht angelegt werden',
    'mc_ok_id'              => 'Tabelle wurde erstellte, id wurde angelegt',
    'mc_err_movie'          => 'FEHLER: konnte nicht in Tabelle filme angelegt werden',
    'mc_ok_movie'           => 'filme wurde in Tabelle angelegt',
    'mc_err_genre'          => 'FEHLER: genre konnte nicht in Tabelle angelegt werden',
    'mc_ok_genre'           => 'genre wurde in Tabelle angelegt',
    'mc_err_genre2'         => 'FEHLER: genre2 konnte nicht in Tabelle angelegt werden',
    'mc_ok_genre2'          => 'genre2 wurde in Tabelle angelegt',
    'mc_err_desc'           => 'FEHLER: beschreibung konnt nicht in Tabelle angelegt werden',
    'mc_ok_desc'            => 'beschreibung wurde in Tabelle angelegt',
    'mc_err_release'        => 'FEHLER: kinostart konnte nicht in Tabelle angelegt werden',
    'mc_ok_release'         => 'kinostart wurde in Tabelle angelegt',
    'mc_err_rating'         => 'FEHLER: fsk konnte nicht in Tabelle angelegt werden',
    'mc_ok_rating'          => 'fsk wurde in Tabelle angelegt',
    'mc_err_medium'         => 'FEHLER: medium konnte nicht in Tabelle angelegt werden',
    'mc_ok_medium'          => 'medium wurde in Tabelle angelegt',
    'mc_err_quantity'       => 'FEHLER: anzahl konnte nicht in Tabelle angelegt werden',
    'mc_ok_quantity'        => 'anzahl wurde in Tabelle angelegt',
    'mc_err_format'         => 'FEHLER: format konnte nicht in Tabelle angelegt werden',
    'mc_ok_format'          => 'format wurde in Tabelle angelegt',
    'mc_err_place'          => 'FEHLER: ort konnte nicht in Tabelle angelegt werden',
    'mc_ok_pplace'          => 'ort wurde in Tabelle angelegt',
    'mc_ok_install'         => '<p>Die installation wurde abgeschlossen. Wenn keine Fehlermeldungen ausgegeben wurden, kann der install-Ordner jetzt gelöscht werden.',
    'mc_username'           => 'Benutzername: ',
    'mc_password'           => 'Passwort: ',
    'mc_create'             => 'Benutzer anlegen',
    'mc_ok_create_user'     => 'Benutzer erfolgreich angelegt',
    'mc_err_create_user'    => 'FEHLER: Benutzer konnte nicht angelegt werden',
    
    'mc_use_database'       => 'Konnte Datenbank nicht auswählen'
    );
?>