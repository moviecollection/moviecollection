<?php
    /**
    @author Ruhland Matthias

    @date 16.05.2008
    @file index.php
    
    @brief Create the index-page and include all other pages
    
    @changes:
    08.01.2013    matthias    translated
    08.01.2013    matthias    charset ISO-8859-1 -> UTF-8
    */

    session_start();
    error_reporting(E_ALL);
    include 'links.php';                // File with links
    include 'settings/settings.php';    // File with settings
    include $mc_language;               // Include language

    // show HTML-Header
    echo '<!DOCTYPE HTML PUBLIC "-//W3C//DTD XHTML 4.01 Transitional//EN"' , "\n";
    echo '         "http://www.w3.org/TR/html4/loose.dtd">' , "\n";
    echo '<html>' , "\n";
    echo '<head>' , "\n";
    echo '<title>' , $language['mc_title'] , '</title>' , "\n";
    echo '<link rel="stylesheet" type="text/css" href="settings/moviecollection.css" />' , "\n";
    echo '<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />' , "\n";
    echo '</head>' , "\n";
    echo '<body>' , "\n";

    echo '<div id="root">' , "\n";
    echo '        <div id="index_left">' , "\n";    // Menu on the left side
    include 'settings/menu.php';                    // include the menu
    echo '        </div>' , "\n";
    echo '        <div id="index_middle">' , "\n";  // "Mainframe"
    include 'settings/selection.php';               // Check the page to include
    echo '        </div>' , "\n";
    echo '       <br style="clear:both;" >' , "\n";
    echo '</div>' , "\n";

    echo '</body>' , "\n";
    echo '</html>' , "\n";
?>